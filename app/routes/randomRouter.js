// Khai báo thư viện express
const express = require('express');

// Import randomMiddleware
const randomMiddleware = require('../middlewares/randomMiddleware');

// Tạo router
const randomRouter = express.Router();

// Random number
randomRouter.use('/random-number', randomMiddleware.getRandomNumber, (req, res) => {
    return res.json({
        number: Math.floor(Math.random() * 6) + 1
    })
});

module.exports = { randomRouter }