// khai báo thư viện mongoose
const mongoose = require("mongoose");

// Import userModel
const userModel = require("../models/userModel");

// Import diceHistoryModel
const diceHistoryModel = require('../models/diceHistoryModel');
const {updateDiceHistory} =  require('./diceHistoryController');

// Create new user
const createUser = (req, res) => {
    //B1: Thu thập dữ liệu
    const diceHistoryId = req.params.diceHistoryId;
    console.log(diceHistoryId);

    const body = req.body;
    console.log(body);

    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "diceHistoryId không hợp lệ"
        })
    }
    // Kiểm tra userName
    if (!body.userName) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "userName is not valid!"
        });
    }
    // Kiểm tra firstName
    if (!body.firstName) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "firstName is not valid!"
        });
    }
    // Kiểm tra lastName
    if (!body.lastName) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "lastName is not valid!"
        });
    }
    //B3: Thực hiện khởi tạo mới user
    const newUser = {
        _id: mongoose.Types.ObjectId(),
        userName: body.userName,
        firstName: body.firstName,
        lastName: body.lastName
    }
    userModel.create(newUser, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        // Thêm ID của user mới được tạo vào trong mảng user của dice-history truyền tại params
        diceHistoryModel.findByIdAndUpdate(diceHistoryId, {$push: {user: data._id}}, (err, updateDiceHistory) => {
            if (err) {
                return res.status(500).json({
                    "status": "Error 500: internal server error",
                    "message": err.message
                })
            }
            else {
                return res.status(201).json({
                    "status": "Create new user successfully!",
                    "data": data
                })
            }
        })
    })
};

// Get all user
const getAllUser = (req, res) => {
    //B1: Thu thập dữ liệu
    //B2: Kiểm tra dữ liệu
    //B3: Thực hiện load all user
    userModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Load all user successfully!",
                "data": data
            })
        }
    })
};

// Get all user of dice history
const getAllUserOfDiceHistory = (req, res) => {
    //B1: Thu thập dữ liệu
    const diceHistoryId = req.params.diceHistoryId;

    //B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "diceHistoryId is not valid!"
        })
    }
    //B3: Thực hiện khởi tạo dữ liệu
    diceHistoryModel.findById(diceHistoryId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Get all user of dice history successfully!",
                "data": data
            })
        }
    }).populate("user")
};

// Get user by id
const getUserById = (req, res) => {
    //B1: Thu thập dữ liệu
    const userId = req.params.userId;
    console.log(userId);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "User Id is not valid!"
        })
    }
    // B3: Thực hiện load user theo id
    userModel.findById(userId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Get user by Id successfully!",
                "data": data
            })
        }
    })
};

// Update user by id
const updateUserById = (req, res) => {
    //B1: Thu thập dữ liệu
    const userId = req.params.userId;
    console.log(userId);
    const body = req.body;
    console.log(body);
    //B2: Kiểm tra dữ liệu
    // KIểm tra user Id
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "User Id is not valid!"
        })
    }
    // Kiểm tra userName
    if (!body.userName) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "userName is not valid!"
        });
    }
    // Kiểm tra firstName
    if (!body.firstName) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "firstName is not valid!"
        });
    }
    // Kiểm tra lastName
    if (!body.lastName) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "lastName is not valid!"
        });
    }
    //B3: Thực hiện khởi tạo mới user
    const updateUser = {
        userName: body.userName,
        firstName: body.firstName,
        lastName: body.lastName
    }
    userModel.findByIdAndUpdate(userId, updateUser, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Update user by Id successfully!",
                "data": data
            })
        }
    })
};

// Delete user by id
const deleteUserById = (req, res) => {
    //B1: Thu thập dữ liệu
    const userId = req.params.userId;
    console.log(userId);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "User Id is not valid!"
        })
    }
    // B3: Thực hiện xóa drink theo id
    userModel.findByIdAndDelete(userId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Delete user by Id successfully!",
                "data": data
            })
        }
    })
};

module.exports = {
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById,
    getAllUserOfDiceHistory
}