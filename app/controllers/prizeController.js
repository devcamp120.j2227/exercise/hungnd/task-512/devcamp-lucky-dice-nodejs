// khai báo thư viện mongoose
const mongoose = require("mongoose");

// Import prizeModel
const prizeModel = require('../models/prizeModel');

// Create new prize
const createPrize = (req, res) => {
    //B1: Thu thập dữ liệu
    const body = req.body;
    console.log(body);
    //B2: Kiểm tr dữ liệu
    if (!body.name) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Name is not valid!"
        })
    }

    if (!body.description) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Description is not valid!"
        })
    }
    //B3: Thực hiện tạo mới prize
    const newPrize = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description
    }
    prizeModel.create(newPrize, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: internal server error",
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Create new prize successfully!",
                "data": data
            })
        }
    })
};

// get all prize
const getAllPrize = (req, res) => {
    //B1: Thu thập dữ liệu
    //B2: Kiểm tra dữ liệu
    //B3: Thực hiện load all prize
    prizeModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: internal server error",
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                status: "Load all prize successfully!",
                data: data
            })
        }
    })
};

// Get prize by id
const getPrizeById = (req, res) => {
    //B1: Thu thập dữ liệu
    const prizeId = req.params.prizeId;
    console.log(prizeId);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "prizeId is not valid!"
        })
    }
    // B3: Thực hiện load user theo id
    prizeModel.findById(prizeId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Get prize by Id successfully!",
                "data": data
            })
        }
    })
};

// Update prize by id
const updatePrizeById = (req, res) => {
    //B1: Thu thập dữ liệu
    const prizeId = req.params.prizeId;
    console.log(prizeId);
    const body = req.body;
    console.log(body);
    //B2: Kiểm tra dữ liệu
    // KIểm tra user Id
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "User Id is not valid!"
        })
    }
    if (!body.name) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Name is not valid!"
        })
    }

    if (!body.description) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Description is not valid!"
        })
    }
    //B3: Thực hiện khởi tạo mới user
    const updatePrize = {
        name: body.name,
        description: body.description
    }
    prizeModel.findByIdAndUpdate(prizeId, updatePrize, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Update prize by Id successfully!",
                "data": data
            })
        }
    })
};

// Delete prize by id
const deletePrizeById = (req, res) => {
    //B1: Thu thập dữ liệu
    const prizeId = req.params.prizeId;
    console.log(prizeId);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "prizeId is not valid!"
        })
    }
    // B3: Thực hiện xóa drink theo id
    prizeModel.findByIdAndDelete(prizeId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Delete prize by Id successfully!",
                "data": data
            })
        }
    })
}

module.exports = {
    createPrize,
    getAllPrize,
    getPrizeById,
    updatePrizeById,
    deletePrizeById
}

