// khai báo thư viện mongoose
const mongoose = require("mongoose");

// Import diceHistoryModel
const diceHistoryModel = require("../models/diceHistoryModel");

// Create new DiceHistory 
const createDiceHistory = (req, res) => {
    //B1: Thu thập dữ liệu
    const body = req.body;
    console.log(body);
    //B2: Kiểm tra dữ liệu
    if(!body.dice) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Dice is not valid!"
        });
    }
    //B3: Thao tác với CSDL
    const newDiceHistory = {
        _id: mongoose.Types.ObjectId(),
        dice: body.dice
    }
    diceHistoryModel.create(newDiceHistory, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Create new dice history successfully!",
                "data": data
            })
        }
    })
};

// get all dice history
const getAllDiceHistory = (req, res) => {
    //B1: Thu thập dữ liệu
    //B2: Kiểm tra dữ liệu
    //B3: Thực hiện load all dice history
    diceHistoryModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Load all dice history successfully!",
                "data": data
            })
        }
    })
};

// get dice history by id
const getDiceHistoryById = (req, res) => {
    //B1: Thu thập dữ liệu
    const diceHistoryId = req.params.diceHistoryId;
    console.log(diceHistoryId);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "diceHistoryId is not valid!"
        })
    }
    // B3: Thực hiện load user theo id
    diceHistoryModel.findById(diceHistoryId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Get dice history by Id successfully!",
                "data": data
            })
        }
    })
};

// update dice history by id
const updateDiceHistoryById = (req, res) => {
    //B1: Thu thập dữ liệu
    const diceHistoryId = req.params.diceHistoryId;
    console.log(diceHistoryId);
    const body = req.body;
    console.log(body);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "diceHistoryId is not valid!"
        })
    }

    if(!body.dice) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Dice is not valid!"
        });
    }
    //B3: Thao tác với CSDL
    const updateDiceHistory = {
        dice: body.dice
    }
    diceHistoryModel.findByIdAndUpdate( diceHistoryId, updateDiceHistory, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Update dice history successfully!",
                "data": data
            })
        }
    })
};

// Delete dice history by id
const deleteDiceHistoryById = (req, res) => {
    //B1: Thu thập dữ liệu
    const diceHistoryId = req.params.diceHistoryId;
    console.log(diceHistoryId);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "diceHistoryId is not valid!"
        })
    }
    // B3: Thực hiện xóa drink theo id
    diceHistoryModel.findByIdAndDelete(diceHistoryId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Delete dice history by Id successfully!",
                "data": data
            })
        }
    })
};

module.exports = {
    createDiceHistory,
    getAllDiceHistory,
    getDiceHistoryById,
    updateDiceHistoryById,
    deleteDiceHistoryById
}