const getAllVoucherMiddleware = (req, res, next) => {
    console.log("Get All Voucher!");
    next();
};

const getVoucherMiddleware = (req, res, next) => {
    console.log("Get a Voucher!");
    next();
};

const postVoucherMiddleware = (req, res, next) => {
    console.log("Create a Voucher!");
    next();
};

const putVoucherMiddleware = (req, res, next) => {
    console.log("Update a Voucher!");
    next();
};

const deleteVoucherMiddleware = (req, res, next) => {
    console.log("Delete a Voucher!");
    next();
};
module.exports = {
    getAllVoucherMiddleware,
    getVoucherMiddleware,
    postVoucherMiddleware,
    putVoucherMiddleware,
    deleteVoucherMiddleware
}